TARGET=adasp_serv
OBJECTS=adasp_serv.o

all: $(TARGET) $(LIBS)

AWSROOT=/opt/gnat-2010-x86_64-apple-darwin9.6.0-bin

$(TARGET): adasp_serv.adb
	gnatmake -g -gnat05 -aI$(AWSROOT)/include/aws -I$(AWSROOT)/include/aws/components  -L$(AWSROOT)/lib/aws/native/static -I/opt/gnat-2010-x86_64-apple-darwin9.6.0-bin/include/aws/native -L/opt/zlib-1.2.5/lib $< -largs -lz

%.o: %.adb %.ads
	gnatmake $<

lib%.so: %.o
	/opt/gnat-2010-x86_64-apple-darwin9.6.0-bin/bin/gcc -dynamiclib $< -o $@ /opt/gnat-2010-x86_64-apple-darwin9.6.0-bin//lib/gcc/x86_64-apple-darwin9.6.0/4.3.6/rts-native/adalib/libgnat-2010.dylib 

clean:
	-rm $(OBJECTS) $(TARGET)
