-- from http://homepage.ntlworld.com/ian.sharpe/isharpe/technote/ada_sl.htm

with System; use System;

package Dll is

      type Handle_Type is private;

      procedure Open(Handle : in out Handle_Type;
                     Name   : in     String;
                     Mode   : in     Integer := 0);
      procedure Close(Handle : in out handle_type);
      function Error(Handle : in Handle_Type) return String;

      generic
         type Item_Type is private;
      procedure Sym(Handle : in out Handle_Type;
                    Name   : in     String;
                    Item   :    out Item_Type);

      Dll_Exception : exception;

   private
      type Handle_Type is
      record
         Os_Handle : System.Address;
      end record;

end Dll;
